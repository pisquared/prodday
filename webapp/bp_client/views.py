from flask import render_template
from flask_security import login_required

from shipka.webapp.views import build_local_ctx, hook_user_view
from webapp.bp_client import bp_client


@hook_user_view(".*")
def before_all_user_views(ctx):
    ctx['app_name'] = 'APP'
    ctx['title'] = 'APP'
    ctx['search_models'] = ''
    return ctx


@bp_client.route('/')
@login_required
def index():
    ctx = build_local_ctx()
    return render_template("index.html", **ctx)
